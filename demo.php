<?php include 'captcha.php'; ?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>CAPTCHA Demo Page</title>
		<meta name="description" content="This page is a demonstration of the form CAPTCHA by Adrian Hart">
	</head>
	<body>
		<form method="post" action="demo.php" style="width:400px;margin:60px auto;">
			<fieldset style="padding:10px 20px">
				<?php captcha() ?>
			</fieldset>			
		</form>
	</body>
</html>