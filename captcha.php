<?php 

	session_cache_limiter('private, must-revalidate'); // set the cache limiter to 'private, must-revalidate'
	session_cache_expire(10); // set the cache expire to 10 minutes
	session_start(); // start the session

	function captcha() {


		function calculate_string( $mathString )    {
		    $mathString = trim($mathString);
		    $mathString = preg_replace ('[^0-9\+-\*\/\(\) ]', '', $mathString); 

		    $compute = create_function("", "return (" . $mathString . ");" );
		    return 0 + $compute();
		}

		$firstNumber = rand(1,10);
		$secondNumber = rand(1,6);
		$operator[0] = '+';
		$operator[1] = '-';
		$plusMinus = $operator[rand()%count($operator)];
		$probQuestion = $firstNumber . $plusMinus . $secondNumber;
		$probAnswer = calculate_string($probQuestion);

	    $capCode = '<label id="cap-q" for="answer">';
	    $capCode .= 'What is ';
	    $capCode .= $probQuestion;
	    $capCode .= '?';
	    $capCode .= '</label>';
	    $capCode .= '<input id="cap-a" type="text" name="answer">'; 
		$capCode .= '<input type="submit" name="submit" value="Answer the Question">';

		if(isset($_POST['submit'])) {

			$capAnswer = $_POST['answer']; // retrieve the answer submitted in the form
			$sessQuestion = $_SESSION['question']; // retrieve the stored question
			$sessAnswer = $_SESSION['answer']; // retrieve the stored answer


			if($capAnswer != '') {

				if($capAnswer == $sessAnswer) {
					echo '<p class="success">Congratulations, your answer was correct!</p>';
					echo $capCode; 
				}
				else {
					echo '<p class="error">Sorry, your answer was incorrect. Please try again.</p>';
					echo $capCode;
				}

			}
			else {
				echo '<p class="error">You must answer the question!</p>';
				echo $capCode;
			}


		}
		else {
			echo $capCode;
		}

		if(isset($_SESSION['question']) || isset($_SESSION['answer'])) {
			unset($_SESSION['question']); // delete old session data
			unset($_SESSION['answer']);
		}

		$_SESSION['question'] = $probQuestion; // store the question
		$_SESSION['answer'] = $probAnswer; // store the answer

	}
?>